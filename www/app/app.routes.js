
app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('citation', {
    url: 'citation/random',
		templateUrl: 'app/views/citation.html',
    controller: 'CitationController'
  })

	.state('auteurs', {
    url: 'auteurs',
		templateUrl: 'app/views/admin-auteurs.html',
    controller: 'AuteursController'
  })

	.state('auteur', {
    url: 'auteur/:id',
		templateUrl: 'app/views/admin-auteur.html',
    controller: 'AuteurController'
  });

});
