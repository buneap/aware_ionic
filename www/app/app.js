var app = angular.module('app',['ionic','LocalStorageModule']);

app.run(function($ionicPlatform, $state) {
	$ionicPlatform.ready(function() {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			cordova.plugins.Keyboard.disableScroll(true);

		}
		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}

		$state.go("citation");
	});
});

app.constant('API', 'http://beta.prokonect.fr/api/');
//app.constant('API', 'http://preprod.prokonect.fr/api/');
//~ var API = 'http://10.2.2.22:8000/' :  serveur renaud;
