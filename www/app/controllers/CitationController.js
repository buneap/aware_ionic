app.controller('CitationController'  ,function($scope, CitationService, $state) {

	CitationService.GetRandom().then(function(response) {
		$scope.citation = citations = response.data[0];
		checkLikes(citations, $scope.citation.id);
		console.log($scope.citation);

	});

	 $scope.admin = function(){
         $state.go('auteurs');
     }

	$scope.getRandom=function(){
		CitationService.GetRandom().then(function(response) {
			$scope.class = "";
			$scope.citation = citations = response.data[0];
			checkLikes(citations, $scope.citation.id);

			return $scope.citation;

		});
	}
	
	// met le coeur en rouge si on a cliquer dessus
	function checkLikes(citations, id) {

		like = localStorage.getItem("likes"+id);

		if (like == null) {
			like =0;
		}

		$scope.citation.likes = localStorage.getItem("likes"+id);

		var likes = window.localStorage.getItem("likes"+id);
		if(likes != null) {
			likes = JSON.parse(likes);
			for (var i = 0; i < citations.length; i++) {
				if(citations[i].id == id) {
					$scope.class="red";
				}
			}
		}
	}

	function clickLike(id) {
		$scope.class="red";
		like = localStorage.getItem("likes"+id);

		if(like == null){
			like = 0;
		}
		var nb = parseInt(like);
		like = nb +1;

		$scope.citation.likes = like;
		localStorage.setItem("likes"+id, like);


		return $scope;
	}



	$scope.likes = function(id){
		clickLike(id);
	}



});
