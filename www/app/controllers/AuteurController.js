app.controller('AuteurController', function ($scope, $stateParams, AuteurService, CitationService, $state) {
		
    AuteurService.GetAuteurById($stateParams.id).then(function (response) {
       
        $scope.auteur = response.data;
        $scope.createQuote = function () {
        CitationService.CreateQuote($scope.auteur.id, $scope.citation);
        }
    });
    $scope.versAuteurs = function(id){
         $state.go('auteurs');
     }

});
