app.factory('AuteursService', function ($http, $q, $window, API) {

    var service = {

        GetAllAuteurs: function () {
            var deferred = $q.defer();
            var url = API + "authors";

            $http.get(url)
                .then(function (data) {
                    service.response = data;
                    deferred.resolve(service.response);
                });

            return deferred.promise;
        },
        CreateAuthor: function (name) {
            var deferred = $q.defer();
            var url = API + "authors";

            $http({
                url: url,
                method: "POST",
                data: "name=" + name,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (data) {
                service.response = data;
                deferred.resolve(service.response);
                //$window.location.reload();
            });

            return deferred.promise;
        }


    }
    
    return service;
});