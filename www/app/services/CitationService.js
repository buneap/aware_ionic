app.factory('CitationService', function($http, $q, $window, API) {

    var service = {
        response : false,

        GetRandom: function() {
            var deferred = $q.defer();
            var url = API + "quotes/random";

            $http.get(url)
            .then(function (data) {
                service.response = data;
                deferred.resolve(service.response);
            });
            return deferred.promise;
        },

        CreateQuote: function (id,quote) {
            var deferred = $q.defer();
            var url = API + "quotes/";

            $http.post(url,{"authorId":id,"content":quote})
      			.then(function (data) {
      				service.response = data;
      				deferred.resolve(service.response);
      				//$window.location.reload();

      			});

            return deferred.promise;
        },
        Like: function(id) {
            var deferred = $q.defer();
            var url = API + "quotes/likes/" + id;

            $http.post(url, {"likes":id})
            .then(function (data) {
                service.response = data;
                deferred.resolve(service.response);
            });
            return deferred.promise;
        }
    };

    return service;
});
