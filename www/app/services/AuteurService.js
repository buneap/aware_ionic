app.factory('AuteurService', function ($http, $q, $window, API) {

    var service = {

        GetAuteurById: function (id) {
            var deferred = $q.defer();
            var url = API + "authors/" + id;

            $http.get(url)
                .then(function (data) {
                    service.response = data;
                    deferred.resolve(service.response);
                });

            return deferred.promise;
        },

    }
    return service;
});
